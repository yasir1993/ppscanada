<?php
/*
Plugin Name: Events Manager
Description: The Events Manager is a carefully crafted Events Manager
Version: 6.6.15
Author: Logics Buffer
Author URI: http://logicsbuffer.com
Text Domain: events-manager
License: GPLv2 or later
*/

/*
Copyright 2009-2012 by Modern Tribe Inc and the contributors

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

define( 'TRIBE_EVENTS_FILE', __FILE__ );

// the main plugin class
require_once dirname( __FILE__ ) . '/src/Tribe/Main.php';

Tribe__Events__Main::instance();

register_activation_hook( __FILE__, array( 'Tribe__Events__Main', 'activate' ) );
register_deactivation_hook( __FILE__, array( 'Tribe__Events__Main', 'deactivate' ) );
